const formatTime = (time) => {
    // console.log(time);
    let date = new Date(time);
    // console.log(date);
    let s = String(date.getSeconds()).padStart(2, '0')
        // console.log(s);
    let m = String(date.getMinutes()).padStart(2, '0')
    let h = String(date.getHours()).padStart(2, '0')
    let dd = String(date.getDate()).padStart(2, '0')
    let mm = String(date.getMonth() + 1).padStart(2, '0')
    let yy = String(date.getFullYear()).padStart(2, '0')
    return `${yy}-${mm}-${dd} ${h}:${m}:${s}`
}

export default formatTime;