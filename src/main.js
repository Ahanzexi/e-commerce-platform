import { createApp } from 'vue'
import App from './App.vue'
import router from './router/router.js'
import 'element-plus/dist/index.css'
import './assets/global.css'
import zhCn from 'element-plus/dist/locale/zh-cn.mjs'
import { ElMessage } from 'element-plus'
import ElementPlus from 'element-plus'

const app = createApp(App)
app.use(router)
app.use(ElementPlus, {
  locale: zhCn,
})
app.component('ElMessage', ElMessage)
app.mount('#app')