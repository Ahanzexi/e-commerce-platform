import axios from 'axios'
axios.defaults.baseURL = `http://127.0.0.1:8888/api/private/v1/`
axios.interceptors.request.use(
    (config) => {
        console.log(config);
        if (config.url != '/login') {
            config.headers.Authorization = window.localStorage.getItem("token");
        }
        return config;
    },
    (error) => Promise.reject(error)
)
export default axios