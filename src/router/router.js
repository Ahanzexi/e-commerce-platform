import { createRouter, createWebHistory, } from "vue-router";
const router = createRouter({
        history: createWebHistory(),
        linkActiveClass: 'active',
        linkExactActiveClass: 'exactActive',
        routes: [{
                path: '/',
                redirect: '/login'
            },
            {
                path: '/login',
                component: () =>
                    import ('../components/login.vue'),
            },
            {
                path: '/homes',
                component: () =>
                    import ('../components/homes.vue'),
                children: [{
                        path: 'users',
                        component: () =>
                            import ('../components/user/Users.vue')
                    },
                    {
                        path: 'roles',
                        component: () =>
                            import ('../components/admin/Roles.vue')
                    }, {
                        path: 'goods',
                        component: () =>
                            import ('../components/good/Goods.vue')
                    }, {
                        path: 'rights',
                        component: () =>
                            import ('../components/admin/Rights.vue')
                    }, {
                        path: 'Roles',
                        component: () =>
                            import ('../components/admin/Roles.vue')
                    }, {
                        path: 'categories',
                        component: () =>
                            import ('../components/good/Categories.vue')
                    }, {
                        path: 'params',
                        component: () =>
                            import ('../components/good/Params.vue')
                    }, {
                        path: 'goods',
                        component: () =>
                            import ('../components/good/Goods.vue')
                    }, {
                        path: 'addgoods',
                        component: () =>
                            import ('../components/good/AddGoods.vue')
                    }, {
                        path: 'orders',
                        component: () =>
                            import ('../components/order/Orders.vue')
                    }, {
                        path: 'reports',
                        component: () =>
                            import ('../components/data/Reports.vue')
                    }
                ]
            },
        ]
    })
    //路由守卫
router.beforeEach((to, from, next) => {
    if (to.path == '/login') return next()
    if (window.localStorage.getItem('token')) return next()
    next('/login')
})
export default router;